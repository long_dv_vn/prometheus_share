Ansible Role: Prometheus
============
Version 2.6

Tags
--------
| Tags | Description |  
|-|-|
| install_prometheus  |  Install Prometheus server  |   
| install_alertmanager  |  Install Alertmanager server  |   
| install_node_exporter  | Install Node_exporter  |  
| install_elasticsearch_exporter  | Install elasticsearch_exporter  |
| install_grafana |   Install Grafana v5.4 |
| config_grafana | Edit config Grafana |
| config_prometheus | Edit config Prometheus |
| config_alert | Add rules alert |



Example Playbook
----------------
```
ansible-playbook -Di inventory/lab -u long.dv play.yml --tags="install_node_exporter"
```

